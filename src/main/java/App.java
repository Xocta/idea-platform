import service.TicketService;

import java.util.HashMap;
import java.util.Scanner;

public class App {
    private static TicketService service;

    public static void main(String[] args) {
        // Ответ на 1 вопрос
        System.out.println("Введите путь к файлу tickets.json:");
        Scanner scanner = new Scanner(System.in);
        service = new TicketService(scanner.next());
        HashMap<String, Integer> ans = service.getMinTime("VVO", "TLV");
        for (String carrier : ans.keySet()) {
            System.out.println("Для авиаперевозчика " + carrier + " время перелета составляет " + ans.get(carrier) + " мин.");
        }
        // Ответ на 2 вопрос
        System.out.println("Разница между средней ценой и медианой - " + (service.getAverage("VVO", "TLV") - service.getMedian("VVO", "TLV")));
    }
}
