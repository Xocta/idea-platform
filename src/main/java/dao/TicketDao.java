package dao;

import com.fasterxml.jackson.databind.ObjectMapper;
import lombok.NoArgsConstructor;
import lombok.Value;
import models.TicketList;

import java.io.File;
import java.io.IOException;
import java.util.Properties;

@NoArgsConstructor
public class TicketDao {
    public TicketList getAllTickets(String path) {
        ObjectMapper mapper = new ObjectMapper().findAndRegisterModules();
        try {
            return mapper.readValue(new File(path), TicketList.class);
        } catch (IOException e) {
            System.out.println("IOException in TicketDao, getAllTickets()");
            return null;
        }
    }
}
