package service;

import dao.TicketDao;
import models.TicketList;
import models.TicketModel;

import java.util.*;

public class TicketService {
    private final TicketDao dao = new TicketDao();
    private final TicketList list;

    public TicketService(String path) {
        this.list = dao.getAllTickets(path);
    }

    public HashMap<String, Integer> getMinTime(String firstPlace, String secondPlace) {
        HashMap<String, Integer> mins = new HashMap<>();
        for (TicketModel ticket : list.getTickets()) {
            if (!Objects.equals(ticket.getOrigin(), firstPlace) || !Objects.equals(ticket.getDestination(), secondPlace)) {
                continue;
            }
            if (!mins.containsKey(ticket.getCarrier())) {
                mins.put(ticket.getCarrier(), Integer.MAX_VALUE);
            }
            int delay = (int) (ticket.getArrivalDate().getTime() - ticket.getDepartureDate().getTime()) / 60000;
            mins.put(ticket.getCarrier(), Math.min(mins.get(ticket.getCarrier()), (ticket.getArrivalTime().toSecondOfDay() + delay - ticket.getDepartureTime().toSecondOfDay()) / 60));
        }
        return mins;
    }

    private List<Integer> getPrices(String firstPlace, String secondPlace) {
        List<Integer> prices = new ArrayList<>();
        for (TicketModel ticket : list.getTickets()) {
            if (!Objects.equals(ticket.getOrigin(), firstPlace) || !Objects.equals(ticket.getDestination(), secondPlace)) {
                continue;
            }
            prices.add(ticket.getPrice());
        }
        Collections.sort(prices);
        return prices;
    }

    public double getMedian(String firstPlace, String secondPlace) {
        List<Integer> prices = getPrices(firstPlace, secondPlace);
        if (prices.size() % 2 == 0) {
            return (double) (prices.get(prices.size() / 2 - 1) + prices.get(prices.size() / 2)) / 2;
        }
        return (double) prices.get(prices.size() / 2 + 1);
    }

    public double getAverage(String firstPlace, String secondPlace) {
        int sum = 0;
        List<Integer> prices = getPrices(firstPlace, secondPlace);
        for (int price : prices) {
            sum += price;
        }
        return (double) sum / prices.size();
    }

    public List<Integer> getTest(String firstPlace, String secondPlace) {
        return getPrices(firstPlace, secondPlace);
    }
}
